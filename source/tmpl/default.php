<?php 
// No direct access
defined('_JEXEC') or die; 

$NewsletterLogo = "<svg alt='Newsletter' id='Ebene_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='22px' height='20px' viewBox='0 0 381 247' xml:space='preserve'>
<path stroke='#000000' stroke-miterlimit='10' d='M355.75,16h-30.293L188.25,157L51.044,16H20.75c-6.627,0-12,5.373-12,12v195c0,6.627,5.373,12,12,12h335c6.627,0,12-5.373,12-12V28C367.75,21.373,362.377,16,355.75,16z'/> <polygon stroke='#000000' stroke-miterlimit='10' points='311.822,13.312 188.25,140 64.678,13.312 '/></svg>";

?>
<table>
	<?php 
	if(!empty($oNeusteNewsletter->NeusteNewsletter)) {
		// Zeigt die Liste an
		foreach ($oNeusteNewsletter->NeusteNewsletter as $row) {
			echo '<div style="text-align:left">' . $NewsletterLogo . " " . $row . '<div style="clear:both;"></div>';
		} 
	}?>
</table>