<?php
/**
 * Klasse zum Anzeigen der neusten Newsletter! Joomla-Modul
 * 
 * @package    Joomla.Tutorials
 * @subpackage Modules
 * @link http:// docs.joomla.org/J3.x:Creating_a_simple_module/Developing_a_Basic_Module
 * @license        GNU/GPL, see LICENSE.php
 * mod_bibelversanzeige is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

// Kein direkter Zugang
defined('_JEXEC') or die;
// Die Klasse Bibelvers liest einen zufälligen Bibelvers aus der Datenbank und
// Initialisiert damit das Objekt

use Joomla\CMS\Filter\OutputFilter;
use Joomla\CMS\Factory;

class NeusteNewsletter
{
	// Eigenschaften / Objekte
	public $NeusteNewsletter;
	
	// Die Klasse instanziieren, d.h. ein Objekt erzeugen (oder: Werte aus den Einstellungen holen)
	function __construct(&$params) {
		
		// Einen zufälligen Bibelvers aus der Datenbank lesen
		$this->readNeusteNewsletter($params);
    }
	
	
	public function readNeusteNewsletter(&$params) {
		
		if(empty($params->get('Newsletter'))) {
			$this->NeusteNewsletter = array('Bitte Einstellungen vervollständigen!');
		} else {	
			// Die Datenbank-Verbindung holen
			$db = Factory::getDbo();
			
			// Ein Query-Objekt holen
			$query = $db->getQuery(true);	
			
			// Die Abfrage formulieren 
			$query->select(array('M.subject As Name',  'M.id AS ID', 'S.mail_id AS MailID', 'S.send_date AS Datum', 'L.list_id AS List'));
			$query->from('j3_acym_mail AS M');
			$query->innerJoin('j3_acym_mail_stat AS S ON M.id = S.mail_id');
			$query->innerJoin('j3_acym_mail_has_list AS L ON M.ID = L.mail_id');
			$query->where('L.list_id IN (' . preg_replace('/^\,/','',$params->get('Newsletter')) . ')');
			$query->order('Datum DESC');
			$query->setLimit($params->get('Anzahl'));
			
			// Abfrage laden
			$Abfrage = $db->setQuery($query);
	
			// Liste lesen
			$Liste = $db->loadAssocList();

			// Liste ausgeben
			foreach ($Liste as $row) {
				$Suchen = array("January","February","March","May","June","July","October","December");
				$Ersetzen = array("Januar","Februar","März","Mai","Juni","Juli","Oktober","Dezember");
				
				$NeusteNewsletter[]= "<a title='". utf8_decode($row['Name']) ."' href='" . $params->get('URL') . $row['ID'] ."-" . OutputFilter::stringUrlSafe(utf8_decode($row['Name'])) . ".html?tmpl=component' target='_blank'>". utf8_decode($row['Name']) ."</a> – " . str_replace($Suchen,$Ersetzen,date("F Y",strtotime($row['Datum'])));
			}  
			
			// Liste als Array zurückgeben
			$this->NeusteNewsletter = $NeusteNewsletter;
		}
	}
}
?>
