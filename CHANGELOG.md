# Ziele für 1.1.0
* Code aufräumen und überarbeiten.
* Handling Backend verbessern.

# 1.0.8 08.08.2023
* Verbesserung: An neuste AcyMailing-Version angepasst.
# 1.0.7 20.10.2022
* Verbesserung: Design an JDownloads angepasst.
* Verbesserung: HTML-Struktur verbessert.
# 1.0.5 16.02.2022
* Fehler: Mann muss für die Link-Bildung und Titel das Subject (Betreff statt Kampagnenname)  nehmen, sonst funktioniert der Link nicht und der Name ist der interne Kampagnenname.
* Verbesserung: Sendedatum wird angezeigt.
# 1.0.3 15.10.2021
* Neu: funktioniert auf unterschiedlichen Webseiten #1
* Verbesserung: Zeigt ein Newsletter-Symbol an
* Verbesserung: Ist konsequent Objektorientiert
* Verbesserung: Nutzt Joomla Update-Server.
